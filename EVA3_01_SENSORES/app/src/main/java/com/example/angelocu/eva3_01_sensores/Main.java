package com.example.angelocu.eva3_01_sensores;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

public class Main extends AppCompatActivity implements ListView.OnItemClickListener{

    ListView listViewSensor;
    TextView textViewMostrar;
    List<Sensor> sensorList;
    SensorManager sensorManager;
    String[] asSensor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listViewSensor = findViewById(R.id.lvListaSens);
        textViewMostrar = findViewById(R.id.tvMostrar);
        sensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        sensorList = sensorManager.getSensorList(Sensor.TYPE_ALL);
        asSensor = new String[sensorList.size()];
        int i = 0;
        for (Sensor sSensor: sensorList) {
            asSensor[i] = sSensor.getName() + " " +sSensor.getVendor();
            i++;
        }
        listViewSensor.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,asSensor));
        listViewSensor.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Sensor sSensor = sensorList.get(position);
        textViewMostrar.setText("Rango Max: " + sSensor.getMaximumRange() + "\n"
        + "Demora: "+ sSensor.getMinDelay() + "\n"
        + "Consumo: "+ sSensor.getPower() + "\n"
        + "Resolución: " + sSensor.getResolution() + "\n");
    }
}
