package com.example.angelocu.eva3_03_bluetooth;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.Toolbar;

import java.util.Set;

public class Main extends AppCompatActivity {

    ListView lvPaired, lvToPair;
    BluetoothAdapter btAdapter;
    android.support.v7.widget.Toolbar toolbar;
    ArrayAdapter<String> aaNuevos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lvPaired = findViewById(R.id.lvPaired);
        lvToPair = findViewById(R.id.lvPair);
        toolbar = findViewById(R.id.miTool);
        setSupportActionBar(toolbar);
        toolbar.setLogo(R.drawable.ic_bluetooth);
        toolbar.setTitle("BLUETOOTH FINDER");
        toolbar.setTitleTextColor(Color.WHITE);
        btAdapter = BluetoothAdapter.getDefaultAdapter();
        LlenarListaDisp();
    }

    @Override
    protected void onStart() {
        super.onStart();
        //VERIFICAR SI ESTA ENCENDIDO EL BLUETOOH
        //sino se pude para activarlo
        if (!btAdapter.isEnabled()){
            Intent activarBlue = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(activarBlue,1000);

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==1000){//PANTALLA DE ACTIVACION DE BLUETOOTH
            if (requestCode == Activity.RESULT_OK){//activado
                Toast.makeText(this,"Activado", Toast.LENGTH_SHORT).show();
            }else {//no activado
                Toast.makeText(this, "No activado", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void onClick(View v){
        //primero verificar si no hay busqueda en proeceso
        if (btAdapter.isDiscovering()){
            btAdapter.cancelDiscovery();
        } else {
            btAdapter.startDiscovery();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (btAdapter != null){
            btAdapter.cancelDiscovery();
        }
        unregisterReceiver(broadcastReceiver);
    }

    public void LlenarListaDisp(){
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1);
        lvPaired.setAdapter(arrayAdapter);
        //*
        // PARA LOS NO CONECTADOS
        //
        // */

        //broadcast reciver
        aaNuevos = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1);
        lvToPair.setAdapter(aaNuevos);
        IntentFilter inFiltro = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(broadcastReceiver,inFiltro);
        IntentFilter intentFilter2 = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        registerReceiver(broadcastReceiver,intentFilter2);


        Set<BluetoothDevice> deviceSet = btAdapter.getBondedDevices();
        if (deviceSet.size() > 0){
            for (BluetoothDevice bluetoothDevice: deviceSet) {
                arrayAdapter.add(bluetoothDevice.getName()+ "\n" + bluetoothDevice.getAddress());
            }
        } else {
            arrayAdapter.add("NO SE ENCONTRAROS DISPOSITIVOS");
        }
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String sAction = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(sAction)){
                BluetoothDevice bluetoothDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if (bluetoothDevice.getBondState() != BluetoothDevice.BOND_BONDED){
                    aaNuevos.add(bluetoothDevice.getName() + "\n" + bluetoothDevice.getAddress());
                }
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(sAction)){
                if (aaNuevos.getCount() == 0){
                    aaNuevos.add("No hay dispositivos");
                }
            }

        }
    };
}
